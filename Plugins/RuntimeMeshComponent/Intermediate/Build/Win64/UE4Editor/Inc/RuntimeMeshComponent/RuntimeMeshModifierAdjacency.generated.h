// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FRuntimeMeshRenderableMeshData;
#ifdef RUNTIMEMESHCOMPONENT_RuntimeMeshModifierAdjacency_generated_h
#error "RuntimeMeshModifierAdjacency.generated.h already included, missing '#pragma once' in RuntimeMeshModifierAdjacency.h"
#endif
#define RUNTIMEMESHCOMPONENT_RuntimeMeshModifierAdjacency_generated_h

#define ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_SPARSE_DATA
#define ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCalculateTessellationIndices);


#define ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCalculateTessellationIndices);


#define ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURuntimeMeshModifierAdjacency(); \
	friend struct Z_Construct_UClass_URuntimeMeshModifierAdjacency_Statics; \
public: \
	DECLARE_CLASS(URuntimeMeshModifierAdjacency, URuntimeMeshModifier, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RuntimeMeshComponent"), NO_API) \
	DECLARE_SERIALIZER(URuntimeMeshModifierAdjacency)


#define ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_INCLASS \
private: \
	static void StaticRegisterNativesURuntimeMeshModifierAdjacency(); \
	friend struct Z_Construct_UClass_URuntimeMeshModifierAdjacency_Statics; \
public: \
	DECLARE_CLASS(URuntimeMeshModifierAdjacency, URuntimeMeshModifier, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RuntimeMeshComponent"), NO_API) \
	DECLARE_SERIALIZER(URuntimeMeshModifierAdjacency)


#define ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URuntimeMeshModifierAdjacency(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URuntimeMeshModifierAdjacency) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URuntimeMeshModifierAdjacency); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URuntimeMeshModifierAdjacency); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URuntimeMeshModifierAdjacency(URuntimeMeshModifierAdjacency&&); \
	NO_API URuntimeMeshModifierAdjacency(const URuntimeMeshModifierAdjacency&); \
public:


#define ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URuntimeMeshModifierAdjacency(URuntimeMeshModifierAdjacency&&); \
	NO_API URuntimeMeshModifierAdjacency(const URuntimeMeshModifierAdjacency&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URuntimeMeshModifierAdjacency); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URuntimeMeshModifierAdjacency); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URuntimeMeshModifierAdjacency)


#define ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_PRIVATE_PROPERTY_OFFSET
#define ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_12_PROLOG
#define ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_PRIVATE_PROPERTY_OFFSET \
	ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_SPARSE_DATA \
	ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_RPC_WRAPPERS \
	ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_INCLASS \
	ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_PRIVATE_PROPERTY_OFFSET \
	ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_SPARSE_DATA \
	ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_INCLASS_NO_PURE_DECLS \
	ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RUNTIMEMESHCOMPONENT_API UClass* StaticClass<class URuntimeMeshModifierAdjacency>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProceduralMap_Plugins_RuntimeMeshComponent_Source_RuntimeMeshComponent_Public_Modifiers_RuntimeMeshModifierAdjacency_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
