# Procedural Map #

This is a personal Unreal Engine Project to create a controllable procedural landscape using a combination of different noises.

Currently it uses Brownian noise along with a form of Voronoi Noise to create a landscape with different geological features.

 C++ files are in the source folder. The rest of the files are all used when running the UE4 editor.