#pragma once
#include <ProceduralMap\Voronoi\VoronoiLayer.h>
#include <ProceduralMap\Biomes\BiomeLayer.h>
#include "ProceduralTile.generated.h"

UCLASS()
class PROCEDURALMAP_API UProceduralTile : public UObject {
	GENERATED_BODY()
public:
	UProceduralTile();
	void CreateTile(int xOffset, int yOffset, int tileSize, float faceSize, const UVoronoiLayer& voronoiLayer, const UBiomeLayer& biomes, const FVector2D& seed, float scaleFactor, int numberOfOctaves);
	TArray<float> HeightMap;
	TArray<FVector> Normals;
	TArray<FVector> Vertices;
	TArray<FVector2D> UVs;
	TArray<int> Triangles;
	TArray<FLinearColor> Colors;

protected:
	int XOffset;
	int YOffset;
	int TileSize;
	int TileSizeWithBuffer;
	float ScaleFactor;
	float FaceSize;
	int NumberOfOctaves;
	const UVoronoiLayer* Layer;
	const UBiomeLayer* Biomes;
	const FVector2D* Seed;
	int NumberOfVertices;
	int NumberOfHeights;

	FVector GetNormalForFace(const FVector& a, const FVector& b, const FVector& c);
	void UpdateNormals(int x, int y);
	FVector GetVertex(int x, int y);
	float GetHeight(int x, int y);
	void CreateHeightMap();
	void CreateTriangles();
	float GetAmplitude(int x, int y, float ampl);
	void UpdateNormal(int x, int y, const FVector& normal);
	bool IsBuffer(int x, int y);
};