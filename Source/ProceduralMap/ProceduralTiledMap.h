#pragma once

#include "CoreMinimal.h"
#include "ProceduralMeshComponent.h"
#include "GameFramework/Actor.h"
#include <ProceduralMap\Octave.h>
#include <ProceduralMap\Voronoi\VoronoiFeature.h>
#include <ProceduralMap\Voronoi\VoronoiFeatureInfo.h>
#include <ProceduralMap\Voronoi\VoronoiFeatureType.h>
#include <ProceduralMap\Voronoi\MinorVoronoiFeatureType.h>
#include <ProceduralMap\Voronoi\MinorVoronoiFeatureInfo.h>
#include <ProceduralMap\Voronoi\MinorVoronoiFeature.h>
#include <ProceduralMap\TileComponents.h>
#include <ProceduralMap\Voronoi\VoronoiLayer.h>
#include "ProceduralTiledMap.generated.h"

UCLASS()
class PROCEDURALMAP_API AProceduralTiledMap : public AActor {
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite)
		int NumberOfOctaves{ 6 };

	UPROPERTY(BlueprintReadWrite)
		int TileSize{ 512 };

	UPROPERTY(BlueprintReadWrite)
		float FaceSize{ 100 };

	UPROPERTY(BlueprintReadWrite)
		int NumberOfVoronoiCells{ 4 };

	UPROPERTY(BlueprintReadWrite)
		int MinorFeaturesPerVoronoiFeature{ 4 };

	UPROPERTY(BlueprintReadWrite)
		int PerturbationFactor{ 50 };

	UPROPERTY(BlueprintReadWrite)
		int PerturbationFrequency{ 8 };

	UPROPERTY(BlueprintReadWrite)
		float ScaleFactor{ 5000 };

	UPROPERTY(BlueprintReadWrite)
		int NumberOfTiles{ 1 };

	UPROPERTY(BlueprintReadWrite)
		FTileComponents Components;

	UFUNCTION(BlueprintCallable)
	void CreateMap();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	int NumberOfTilesSquared;
	int TotalNumberOfVoronoiFeatures;
	int TotalMapSize;

};