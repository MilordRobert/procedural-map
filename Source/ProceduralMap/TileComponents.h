#pragma once
#include "TileComponents.generated.h"

USTRUCT(BlueprintType)
struct FTileComponents {
    GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<float> HeightMap;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FVector> Normals;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FVector> Vertices;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FVector2D> UVs;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<int> Triangles;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FColor> Colors;

	FTileComponents() {}
	
	FTileComponents(int size) {
		int totalSize = size * size;
		int totalTriangles = (size - 1) * (size - 1) * 6;

		HeightMap.Empty();
		Normals.Empty();
		Vertices.Empty();
		UVs.Empty();
		Colors.Empty();
		Triangles.Empty();
		HeightMap.Reserve(totalSize);
		Normals.SetNumUninitialized(totalSize);
		Vertices.Reserve(totalSize);
		UVs.Reserve(totalSize);
		Colors.Reserve(totalSize);
		Triangles.Reserve(totalTriangles);
	}
};