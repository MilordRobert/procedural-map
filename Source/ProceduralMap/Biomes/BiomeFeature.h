#pragma once

#include "BiomeType.h"
#include "BiomeFeature.generated.h"

USTRUCT(BlueprintType)
struct FBiomeFeature {
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FVector Location;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EBiomeType Type;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float InnerInfluenceSphere;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FColor FeatureColor;
};