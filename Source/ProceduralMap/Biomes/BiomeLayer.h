#pragma once

#include "BiomeFeature.h"
#include "BiomeType.h"
#include "BiomeInfo.h"
#include "BiomeLayer.generated.h"

UCLASS()
class PROCEDURALMAP_API UBiomeLayer : public UObject {
	GENERATED_BODY()
public:

	UBiomeLayer();
	FLinearColor GetColor(float x, float y) const;
	void InitLayer(int numberOfCells, float faceSize, int tileSize, float perturbationFrequency, float perturbationFactor, const FVector2D& seed);
	void AddBiome(EBiomeType biome, FVector location);

private:
	TArray<FBiomeFeature> BiomeFeatures;

	UDataTable* Biomes;

	const FVector2D* Seed;

	float SizeOfVoronoiCell;
	int NumberOfFacesPerVoronoiCell;
	int NumberOfVoronoiCells;
	float PerturbationFactor;
	float PerturbationFrequency;
	float FaceSize;
	float TileSize;

	FBiomeInfo* GetBiomeInfoByType(EBiomeType type) const;
	float GetInfluenceFactor(float distance, float maxDistance) const;
};

