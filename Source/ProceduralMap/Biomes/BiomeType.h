#pragma once

#include "BiomeType.generated.h"

UENUM(BlueprintType)
enum class EBiomeType : uint8 {
    Grass 	UMETA(DisplayName = "Grass"),
    Dirt 	UMETA(DisplayName = "Dirt"),
    Forest	UMETA(DisplayName = "Forest"),
    Sand	UMETA(DisplayName = "Sand"),
    FinalValue	UMETA(DisplayName = "FinalValue")
};
