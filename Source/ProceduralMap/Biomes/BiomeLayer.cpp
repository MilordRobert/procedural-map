#include "BiomeLayer.h"
#include <Runtime\CoreUObject\Public\UObject\ConstructorHelpers.h>

UBiomeLayer::UBiomeLayer() {
	ConstructorHelpers::FObjectFinder<UDataTable> biomes(TEXT("DataTable'/Game/Environment/Data/BiomeTable.BiomeTable'"));
	if (biomes.Succeeded()) {
		Biomes = biomes.Object;
	}
}

FLinearColor UBiomeLayer::GetColor(float x, float y) const {
	float xPerturbed = x + FMath::PerlinNoise1D(Seed->X + y / (float)TileSize * PerturbationFrequency) * PerturbationFactor;
	float yPerturbed = y + FMath::PerlinNoise1D(Seed->Y + x / (float)TileSize * PerturbationFrequency) * PerturbationFactor;

	int xVoronoi = xPerturbed / NumberOfFacesPerVoronoiCell + 1;
	int yVoronoi = yPerturbed / NumberOfFacesPerVoronoiCell + 1;

	FVector location{ xPerturbed * FaceSize, yPerturbed * FaceSize, 0 };
	FLinearColor color;
	float closestDistance = TNumericLimits<float>::Max();

	for (int i = -1; i <= 1; ++i) {
		for (int j = -1; j <= 1; ++j) {
			int index = (xVoronoi + i) * NumberOfVoronoiCells + yVoronoi + j;
			if (index < BiomeFeatures.Num() && index >= 0) {
				FBiomeFeature feature = BiomeFeatures[index];
				float distance = FVector::DistXY(feature.Location, location);

				if (distance < SizeOfVoronoiCell) {
					closestDistance = distance;
					FBiomeInfo* info = GetBiomeInfoByType(feature.Type);
					
					color += (FLinearColor)info->FeatureColor * GetInfluenceFactor(distance, SizeOfVoronoiCell);
				}
			}
		}
	}
	return color.GetClamped();
}

void UBiomeLayer::InitLayer(int numberOfCells, float faceSize, int tileSize, float perturbationFrequency, float perturbationFactor, const FVector2D& seed) {
	BiomeFeatures.Empty();
	Seed = &seed;
	FaceSize = faceSize;
	TileSize = tileSize;
	NumberOfVoronoiCells = numberOfCells;
	PerturbationFactor = perturbationFactor;
	PerturbationFrequency = perturbationFrequency;
	NumberOfFacesPerVoronoiCell = tileSize / NumberOfVoronoiCells;
	SizeOfVoronoiCell = FaceSize * NumberOfFacesPerVoronoiCell;
	NumberOfVoronoiCells += 2; // Adding 2 to get an invisible feature off the edge of the map to make it look more continuous.
	int totalNumberOfVoronoiCells = NumberOfVoronoiCells * NumberOfVoronoiCells;
	BiomeFeatures.Reserve(totalNumberOfVoronoiCells);
}

void UBiomeLayer::AddBiome(EBiomeType biome, FVector location) {
	FBiomeInfo* info = GetBiomeInfoByType(biome);
	BiomeFeatures.Emplace(FBiomeFeature{ location, biome, info->InnerInfluenceSphere, info->FeatureColor });
}

FBiomeInfo* UBiomeLayer::GetBiomeInfoByType(EBiomeType type) const {
	FString typeName = UEnum::GetValueAsString<EBiomeType>(type);
	return Biomes->FindRow<FBiomeInfo>(FName(*typeName), FString(TEXT("Biome Info")));
}

float UBiomeLayer::GetInfluenceFactor(float distance, float maxDistance) const {
	return 1 - distance / maxDistance;
}
