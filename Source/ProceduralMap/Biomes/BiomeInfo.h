#pragma once

#include "Engine/DataTable.h"
#include "BiomeInfo.generated.h"

USTRUCT(BlueprintType)
struct FBiomeInfo : public FTableRowBase {
    GENERATED_USTRUCT_BODY()

        UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float InnerInfluenceSphere;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        FColor FeatureColor;
};