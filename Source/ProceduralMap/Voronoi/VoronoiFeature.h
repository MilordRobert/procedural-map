#pragma once

#include "VoronoiFeatureType.h"
#include "VoronoiFeature.generated.h"

USTRUCT(BlueprintType)
struct FVoronoiFeature {
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector Location;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Influence;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Offset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EVoronoiFeatureType FeatureType;
};