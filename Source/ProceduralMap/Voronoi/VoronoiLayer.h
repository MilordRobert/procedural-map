#pragma once
#include "VoronoiFeature.h"
#include "VoronoiFeatureInfo.h"
#include "VoronoiFeatureType.h"
#include "MinorVoronoiFeatureType.h"
#include "MinorVoronoiFeatureInfo.h"
#include "MinorVoronoiFeature.h"
#include <ProceduralMap\Biomes\BiomeLayer.h>
#include "VoronoiLayer.generated.h"

UCLASS()
class PROCEDURALMAP_API UVoronoiLayer : public UObject{
	GENERATED_BODY()
public:

	UVoronoiLayer();
	float GetInfluence(float x, float y) const;
	void CreateLayer(int numberOfCells, int minorFeaturesPerCell, float faceSize, int tileSize, float perturbationFrequency, float perturbationFactor, const FVector2D& seed, UBiomeLayer& biomes);
	
private:
	TArray<FVoronoiFeature> VoronoiFeatures;
	TArray<FMinorVoronoiFeature> MinorFeatures;

	UDataTable* MajorVoronoiFeatures;
	UDataTable* MinorVoronoiFeatures;

	const FVector2D* Seed;

	float SizeOfVoronoiCell;
	float SizeOfMinorFeature;
	int NumberOfFacesPerVoronoiCell;
	int NumberOfMinorFeatures;
	int NumberOfFacesPerMinorFeature;
	int NumberOfVoronoiCells;
	int MinorFeaturesPerVoronoiFeature;
	float PerturbationFactor;
	float PerturbationFrequency;
	float FaceSize;
	float TileSize;

	// The display names for enums only exist while in the editor, so use this dictionary to get the names at runtime.
	TMap<EVoronoiFeatureType, FName> VoronoiFeatureToName{
		{EVoronoiFeatureType::Beach, FName("Beach")},
		{EVoronoiFeatureType::GiantMountain, FName("GiantMountain")},
		{EVoronoiFeatureType::Grassland, FName("Grassland")},
		{EVoronoiFeatureType::Hill, FName("Hill")},
		{EVoronoiFeatureType::LargeHill, FName("LargeHill")},
		{EVoronoiFeatureType::Mountain, FName("Mountain")},
		{EVoronoiFeatureType::Ocean, FName("Ocean")},
		{EVoronoiFeatureType::FinalValue, FName("FinalValue")}
	};

	TMap<EMinorVoronoiFeatureType, FName> MinorVoronoiFeatureToName{
		{EMinorVoronoiFeatureType::Cliff, FName("Cliff")},
		{EMinorVoronoiFeatureType::Dip, FName("Dip")},
		{EMinorVoronoiFeatureType::Mound, FName("Mound")},
		{EMinorVoronoiFeatureType::Peak, FName("Peak")},
		{EMinorVoronoiFeatureType::Ravine, FName("Ravine")},
		{EMinorVoronoiFeatureType::FinalValue, FName("FinalValue")}
	};

	void CreateVoronoiFeatures(UBiomeLayer& biomes);
	void CreateMinorFeatures(int majorX, int majorY, EVoronoiFeatureType majorType);
	FVoronoiFeatureInfo* GetFeatureInfoByType(EVoronoiFeatureType type) const;
	FMinorVoronoiFeatureInfo* GetMinorFeatureInfoByType(EMinorVoronoiFeatureType type) const;
	float GetInfluenceFactor(float distance, float maxDistance, float dropOff) const;

	FORCEINLINE EVoronoiFeatureType GetRandomFeatureType() const;
	FORCEINLINE EBiomeType GetRandomBiome() const;
	FORCEINLINE EMinorVoronoiFeatureType GetRandomMinorFeature() const;
};

