#pragma once

#include "VoronoiFeatureType.generated.h"

UENUM(BlueprintType)
enum class EVoronoiFeatureType : uint8 {
    Grassland 	UMETA(DisplayName = "Grassland"),
    Mountain 	UMETA(DisplayName = "Mountain"),
    Ocean	UMETA(DisplayName = "Ocean"),
    Beach	UMETA(DisplayName = "Beach"),
    GiantMountain	UMETA(DisplayName = "Giant Mountain"),
    LargeHill	UMETA(DisplayName = "Large Hill"),
    Hill    UMETA(DisplayName = "Hill"),
    FinalValue    UMETA(DisplayName = "FinalValue"),
};
