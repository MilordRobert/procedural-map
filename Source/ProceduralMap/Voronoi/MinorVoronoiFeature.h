#pragma once

#include "MinorVoronoiFeatureType.h"
#include "MinorVoronoiFeature.generated.h"

USTRUCT(BlueprintType)
struct FMinorVoronoiFeature {
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector Location;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Influence;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EMinorVoronoiFeatureType FeatureType;
};