#include "VoronoiLayer.h"
#include <Runtime\CoreUObject\Public\UObject\ConstructorHelpers.h>

UVoronoiLayer::UVoronoiLayer() {
	ConstructorHelpers::FObjectFinder<UDataTable> voronoiFeatures(TEXT("DataTable'/Game/Environment/Data/VoronoiFeatureTable.VoronoiFeatureTable'"));
	if (voronoiFeatures.Succeeded()) {
		MajorVoronoiFeatures = voronoiFeatures.Object;
	}

	ConstructorHelpers::FObjectFinder<UDataTable> minorFeatures(TEXT("DataTable'/Game/Environment/Data/MinorVoronoiFeatureTable.MinorVoronoiFeatureTable'"));
	if (minorFeatures.Succeeded()) {
		MinorVoronoiFeatures = minorFeatures.Object;
	}
}

void UVoronoiLayer::CreateLayer(int numberOfCells, int minorFeaturesPerCell, float faceSize, int tileSize, float perturbationFrequency, float perturbationFactor, const FVector2D& seed, UBiomeLayer& biomes) {
	VoronoiFeatures.Empty();
	MinorFeatures.Empty();
	Seed = &seed;
	FaceSize = faceSize;
	TileSize = tileSize;
	NumberOfVoronoiCells = numberOfCells;
	MinorFeaturesPerVoronoiFeature = minorFeaturesPerCell;
	NumberOfMinorFeatures = NumberOfVoronoiCells * MinorFeaturesPerVoronoiFeature;
	PerturbationFactor = perturbationFactor;
	PerturbationFrequency = perturbationFrequency;
	NumberOfFacesPerVoronoiCell = tileSize / NumberOfVoronoiCells;
	SizeOfVoronoiCell = FaceSize * NumberOfFacesPerVoronoiCell;
	SizeOfMinorFeature = SizeOfVoronoiCell / MinorFeaturesPerVoronoiFeature;
	NumberOfFacesPerMinorFeature = tileSize / NumberOfMinorFeatures;
	NumberOfVoronoiCells += 2; // Adding 2 to get an invisible feature off the edge of the map to make it look more continuous.
	NumberOfMinorFeatures = NumberOfVoronoiCells * MinorFeaturesPerVoronoiFeature;
	CreateVoronoiFeatures(biomes);
}

void UVoronoiLayer::CreateVoronoiFeatures(UBiomeLayer& biomes) {
	int totalNumberOfVoronoiCells = NumberOfVoronoiCells * NumberOfVoronoiCells;
	VoronoiFeatures.Reserve(totalNumberOfVoronoiCells);
	MinorFeatures.SetNumUninitialized(NumberOfMinorFeatures * NumberOfMinorFeatures, false);

	for (int i = 0; i < NumberOfVoronoiCells; ++i) {
		float xOffset = i * SizeOfVoronoiCell + SizeOfVoronoiCell * 0.2 - SizeOfVoronoiCell;
		for (int j = 0; j < NumberOfVoronoiCells; ++j) {
			float yOffset = j * SizeOfVoronoiCell + SizeOfVoronoiCell * 0.2 - SizeOfVoronoiCell;
			float x = FMath::FRand() * SizeOfVoronoiCell * 0.8;
			float y = FMath::FRand() * SizeOfVoronoiCell * 0.8;

			float biomeX = FMath::FRand() * SizeOfVoronoiCell * 0.8;
			float biomeY = FMath::FRand() * SizeOfVoronoiCell * 0.8;
			FVector biomeLocation(biomeX + xOffset, biomeY + yOffset, 0);

			EVoronoiFeatureType type;
			if (i == 0 && j == 0) {
				type = GetRandomFeatureType();
			} else if (i == 0) {
				FVoronoiFeatureInfo* lastInfo = GetFeatureInfoByType(VoronoiFeatures[j - 1].FeatureType);

				if (lastInfo != nullptr) {
					type = lastInfo->ValidNeighbours.Array()[FMath::RandRange(0, lastInfo->ValidNeighbours.Num() - 1)];
				} else {
					type = UVoronoiLayer::GetRandomFeatureType();
				}

			} else if (j == 0) {
				FVoronoiFeatureInfo* lastInfo = GetFeatureInfoByType(VoronoiFeatures[(i - 1) * NumberOfVoronoiCells].FeatureType);

				if (lastInfo != nullptr) {
					type = lastInfo->ValidNeighbours.Array()[FMath::RandRange(0, lastInfo->ValidNeighbours.Num() - 1)];
				} else {
					type = GetRandomFeatureType();
				}
			} else {
				FVoronoiFeatureInfo* lastInfoY = GetFeatureInfoByType(VoronoiFeatures[i * NumberOfVoronoiCells + j - 1].FeatureType);
				FVoronoiFeatureInfo* lastInfoX = GetFeatureInfoByType(VoronoiFeatures[(i - 1) * NumberOfVoronoiCells + j].FeatureType);

				if (lastInfoX != nullptr && lastInfoY != nullptr) {
					TSet<EVoronoiFeatureType> possible = lastInfoX->ValidNeighbours.Intersect(lastInfoY->ValidNeighbours);
					if (possible.Num() != 0) {
						type = possible.Array()[FMath::RandRange(0, possible.Num() - 1)];
					} else {
						type = GetRandomFeatureType();
					}
				} else {
					type = GetRandomFeatureType();
				}
			}

			FVoronoiFeatureInfo* info = GetFeatureInfoByType(type);
			float influence{ 0 };
			if (info != nullptr) {
				influence = FMath::FRandRange(info->LowerInfluenceRange, info->UpperInfluenceRange);
				EBiomeType biome = info->PossibleBiomes.Array()[FMath::RandRange(0, info->PossibleBiomes.Num() - 1)];
				biomes.AddBiome(biome, biomeLocation);
			} else {
				influence = FMath::FRandRange(-1.0, 1.0);
				EBiomeType biome = GetRandomBiome();
				biomes.AddBiome(biome, biomeLocation);
			}
			VoronoiFeatures.Emplace(FVoronoiFeature{ FVector{ x + xOffset, y + yOffset, 0 }, influence, 0, type });
			CreateMinorFeatures(i, j, type);
		}
	}
}

EVoronoiFeatureType UVoronoiLayer::GetRandomFeatureType() const {
	return EVoronoiFeatureType(FMath::RandRange(0, (uint8)EVoronoiFeatureType::FinalValue - 1));
}

EBiomeType UVoronoiLayer::GetRandomBiome() const {
	return EBiomeType(FMath::RandRange(0, (uint8)EBiomeType::FinalValue - 1));
}

EMinorVoronoiFeatureType UVoronoiLayer::GetRandomMinorFeature() const {
	return EMinorVoronoiFeatureType(FMath::RandRange(0, (uint8)EMinorVoronoiFeatureType::FinalValue - 1));
}

FVoronoiFeatureInfo* UVoronoiLayer::GetFeatureInfoByType(EVoronoiFeatureType type) const {
	const FName* typeName = VoronoiFeatureToName.Find(type);
	return MajorVoronoiFeatures->FindRow<FVoronoiFeatureInfo>(*typeName, FString(TEXT("Voronoi Feature")));
}

FMinorVoronoiFeatureInfo* UVoronoiLayer::GetMinorFeatureInfoByType(EMinorVoronoiFeatureType type) const {
	const FName* typeName = MinorVoronoiFeatureToName.Find(type);
	return MinorVoronoiFeatures->FindRow<FMinorVoronoiFeatureInfo>(*typeName, FString(TEXT("Minor Voronoi Feature")));
}

void UVoronoiLayer::CreateMinorFeatures(int majorX, int majorY, EVoronoiFeatureType majorType) {
	int rowIndexOffset = majorX * MinorFeaturesPerVoronoiFeature;
	int columnIndexOffset = majorY * MinorFeaturesPerVoronoiFeature;
	for (int i = 0; i < MinorFeaturesPerVoronoiFeature; ++i) {
		int rowIndex = i + rowIndexOffset;
		int xOffset = rowIndex * SizeOfMinorFeature + SizeOfMinorFeature * 0.2 - SizeOfMinorFeature * MinorFeaturesPerVoronoiFeature;
		for (int j = 0; j < MinorFeaturesPerVoronoiFeature; ++j) {
			int columnIndex = j + columnIndexOffset;
			int yOffset = columnIndex * SizeOfMinorFeature + SizeOfMinorFeature * 0.2 - SizeOfMinorFeature * MinorFeaturesPerVoronoiFeature;

			float x = FMath::FRand() * SizeOfMinorFeature * 0.8;
			float y = FMath::FRand() * SizeOfMinorFeature * 0.8;

			FVoronoiFeatureInfo* info = GetFeatureInfoByType(majorType);
			EMinorVoronoiFeatureType type;
			if (info != nullptr) {
				int selection = FMath::RandRange(0, info->PossibleSubFeatures.Num() - 1);
				type = info->PossibleSubFeatures.Array()[selection];
			} else {
				type = GetRandomMinorFeature();
			}

			FMinorVoronoiFeatureInfo* minorInfo = GetMinorFeatureInfoByType(type);

			float influence{ 0 };
			if (minorInfo != nullptr) {
				influence = FMath::FRandRange(minorInfo->LowerInfluenceRange, minorInfo->UpperInfluenceRange);
			} else {
				influence = FMath::FRandRange(-1.0, 1.0);
			}
			MinorFeatures[rowIndex * NumberOfMinorFeatures + columnIndex] = FMinorVoronoiFeature{ FVector{ x + xOffset, y + yOffset, 0 }, influence, type };
		}
	}
}

float UVoronoiLayer::GetInfluence(float x, float y) const{
	float xPerturbed = x + FMath::PerlinNoise1D(Seed->X + y / (float)TileSize * PerturbationFrequency) * PerturbationFactor;
	float yPerturbed = y + FMath::PerlinNoise1D(Seed->Y + x / (float)TileSize * PerturbationFrequency) * PerturbationFactor;

	int xVoronoi = xPerturbed / NumberOfFacesPerVoronoiCell + 1;
	int yVoronoi = yPerturbed / NumberOfFacesPerVoronoiCell + 1;

	int xMinor = xPerturbed / NumberOfFacesPerMinorFeature + MinorFeaturesPerVoronoiFeature;
	int yMinor = yPerturbed / NumberOfFacesPerMinorFeature + MinorFeaturesPerVoronoiFeature;

	FVector location{ xPerturbed * FaceSize, yPerturbed * FaceSize, 0 };
	float influence{ 0 };

	for (int i = -1; i <= 1; ++i) {
		for (int j = -1; j <= 1; ++j) {
			int index = (xVoronoi + i) * NumberOfVoronoiCells + yVoronoi + j;
			if (index < VoronoiFeatures.Num() && index >= 0) {
				FVoronoiFeature feature = VoronoiFeatures[index];
				float distance = FVector::DistXY(feature.Location, location);

				if (distance < SizeOfVoronoiCell) {
					float dropOff{ 0.5 };
					FVoronoiFeatureInfo* info = GetFeatureInfoByType(feature.FeatureType);
					if (info != nullptr) {
						dropOff = info->DropOffFactor;
					}
					influence += GetInfluenceFactor(distance, SizeOfVoronoiCell, dropOff) * feature.Influence;
				}
			}

			int minorIndex = (xMinor + i) * NumberOfMinorFeatures + yMinor + j;
			if (minorIndex >= MinorFeatures.Num() || minorIndex < 0) {
				continue;
			}

			FMinorVoronoiFeature minorFeature = MinorFeatures[minorIndex];
			float minorDistance = FVector::DistXY(minorFeature.Location, location);
			FMinorVoronoiFeatureInfo* minorInfo = GetMinorFeatureInfoByType(minorFeature.FeatureType);
			if (minorDistance < SizeOfMinorFeature) {
				float dropOff{ 0.5 };
				if (minorInfo != nullptr) {
					dropOff = minorInfo->DropOffFactor;
				}
				influence += GetInfluenceFactor(minorDistance, SizeOfMinorFeature, dropOff) * minorFeature.Influence;
			}
		}
	}
	return influence;
}

float UVoronoiLayer::GetInfluenceFactor(float distance, float maxDistance, float dropOffFactor) const {
	float factor{ distance / maxDistance };
	float exponentialDropOff = 1 - factor;
	exponentialDropOff *= exponentialDropOff;

	float slowDropOff = 1 - factor * factor;
	return exponentialDropOff * dropOffFactor + slowDropOff * (1 - dropOffFactor);
}