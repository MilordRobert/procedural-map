#pragma once

#include "MinorVoronoiFeatureType.generated.h"

UENUM(BlueprintType)
enum class EMinorVoronoiFeatureType : uint8 {
    Mound 	UMETA(DisplayName = "Mound"),
    Ravine 	UMETA(DisplayName = "Ravine"),
    Dip	    UMETA(DisplayName = "Dip"),
    Peak   UMETA(DisplayName = "Peak"),
    Cliff   UMETA(DisplayName = "Cliff"),
    FinalValue   UMETA(DisplayName = "Final Value")
};
