#pragma once

#include "MinorVoronoiFeatureType.h"
#include "Engine/DataTable.h"
#include "MinorVoronoiFeatureInfo.generated.h"

USTRUCT(BlueprintType)
struct FMinorVoronoiFeatureInfo : public FTableRowBase {
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float LowerInfluenceRange;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float UpperInfluenceRange;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float DropOffFactor;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        FColor FeatureColor;
};