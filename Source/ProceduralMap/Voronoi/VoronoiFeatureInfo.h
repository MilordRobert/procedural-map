#pragma once

#include "MinorVoronoiFeatureType.h"
#include "VoronoiFeatureType.h"
#include <ProceduralMap\Biomes\BiomeType.h>
#include "Engine/DataTable.h"
#include "VoronoiFeatureInfo.generated.h"

USTRUCT(BlueprintType)
struct FVoronoiFeatureInfo : public FTableRowBase {
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float LowerInfluenceRange;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float UpperInfluenceRange;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float DropOffFactor;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        FColor FeatureColor;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        TSet<EVoronoiFeatureType> ValidNeighbours;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        TSet<EMinorVoronoiFeatureType> PossibleSubFeatures;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        TSet<EBiomeType> PossibleBiomes;
};