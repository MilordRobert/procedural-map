#include "ProceduralTile.h"

UProceduralTile::UProceduralTile() {}

void UProceduralTile::CreateTile(int xOffset,
	int yOffset, 
	int tileSize,
	float faceSize,
	const UVoronoiLayer& voronoiLayer,
	const UBiomeLayer& biomes,
	const FVector2D& seed,
	float scaleFactor,
	int numberOfOctaves) {
	XOffset = xOffset;
	YOffset = yOffset;	
	Biomes = &biomes;
	Layer = &voronoiLayer;
	Seed = &seed;
	NumberOfVertices = tileSize * tileSize;
	ScaleFactor = scaleFactor;
	TileSize = tileSize;
	TileSizeWithBuffer = TileSize + 2; // Adding 2 to provide two imaginary faces around the outside to use for matching up the normals of the different tiles.
	NumberOfHeights = TileSizeWithBuffer * TileSizeWithBuffer;
	FaceSize = faceSize;
	NumberOfOctaves = numberOfOctaves;
	CreateHeightMap();
	CreateTriangles();
}

void UProceduralTile::CreateHeightMap() {
	HeightMap.Empty();
	HeightMap.Reserve(NumberOfHeights);
	Colors.Empty();
	Colors.Reserve(NumberOfVertices);
	for (int i = 0; i < TileSizeWithBuffer; ++i) {
		int x = i - 1;
		int worldX = x + XOffset;
		for (int j = 0; j < TileSizeWithBuffer; ++j) {
			int y = j - 1;
			int worldY = y + YOffset;

			float height = GetHeight(worldX, worldY);
			HeightMap.Add(height);

			if (!IsBuffer(x, y)) {
				FLinearColor color = Biomes->GetColor(worldX, worldY);
				Colors.Add(color);
			}
		}
	}
}

void UProceduralTile::CreateTriangles() {
	Normals.Empty();
	Vertices.Empty();
	UVs.Empty();
	Triangles.Empty();
	Normals.SetNumZeroed(NumberOfVertices);
	Vertices.Reserve(NumberOfVertices);
	UVs.Reserve(NumberOfVertices);
	Triangles.Reserve((TileSize - 1) * (TileSize - 1) * 6);

	for (int i = 0; i < HeightMap.Num(); ++i) {
		int x = i / TileSizeWithBuffer - 1;
		int y = i % TileSizeWithBuffer - 1;

		if (!IsBuffer(x, y)) {
			float height = HeightMap[i];

			float back = HeightMap[x * TileSizeWithBuffer + y + 1];
			float front = HeightMap[(x + 2) * TileSizeWithBuffer + y + 1];
			float left = HeightMap[(x + 1) * TileSizeWithBuffer + y];
			float right = HeightMap[(x + 1) * TileSizeWithBuffer + y + 2];

			float errosion = (back + front + left + right - height * 4) * 0.2;
			height += errosion;

			FVector position = FVector{ (x + XOffset) * FaceSize, (y + YOffset) * FaceSize, height };

			Vertices.Add(position);
			UVs.Emplace(x / (float)TileSize, y / (float)TileSize);

			if (x != 0 && y != 0) {
				int index = x * TileSize + y;
				int previousColumn = index - 1;
				int previousRow = index - TileSize;
				int previousRowColumn = previousRow - 1;

				Triangles.Add(index);
				Triangles.Add(previousColumn);
				Triangles.Add(previousRowColumn);

				Triangles.Add(previousRow);
				Triangles.Add(index);
				Triangles.Add(previousRowColumn);
			}
		}

		if (x != -1 && y != -1) {
			UpdateNormals(x, y);
		}
	}

	for (int i = 0; i < Normals.Num(); ++i) {
		Normals[i].Normalize();
	}
}

void UProceduralTile::UpdateNormal(int x, int y, const FVector& normal) {
	if (!IsBuffer(x, y)) {
		Normals[x * TileSize + y] += normal;
	}
}

bool UProceduralTile::IsBuffer(int x, int y) {
	return x <= -1 || x >= TileSize || y <= -1 || y >= TileSize;
}

FVector UProceduralTile::GetVertex(int x, int y) {
	if (!IsBuffer(x, y)) {
		return Vertices[x * TileSize + y];
	}

	int heightIndex = (x + 1) * TileSizeWithBuffer + (y + 1);
	float height = HeightMap[heightIndex];
	return FVector{ (x + XOffset) * FaceSize, (y + YOffset) * FaceSize, height };
}

FVector UProceduralTile::GetNormalForFace(const FVector& a, const FVector& b, const FVector& c) {
	FVector sideAB = b - a;
	FVector sideAC = c - a;
	FVector scaledNormal = FVector::CrossProduct(sideAC, sideAB);
	scaledNormal.Z = FMath::Abs(scaledNormal.Z);
	return scaledNormal;
}

void UProceduralTile::UpdateNormals(int x, int y) {
	int prevX = x - 1;
	int prevY = y - 1;
	FVector current = GetVertex(x, y);
	FVector prevColumn = GetVertex(x, prevY);
	FVector prevRow = GetVertex(prevX, y);
	FVector prevDiagonal = GetVertex(prevX, prevY);

	FVector normalA = GetNormalForFace(current, prevColumn, prevDiagonal);
	FVector normalB = GetNormalForFace(prevRow, current, prevDiagonal);
	FVector combined = normalA + normalB;
	UpdateNormal(x, y, combined);
	UpdateNormal(prevX, y, normalB);
	UpdateNormal(x, prevY, normalA);
	UpdateNormal(prevX, prevY, combined);
}

float UProceduralTile::GetHeight(int x, int y) {
	float voronoi = Layer->GetInfluence(x, y);
	return GetAmplitude(x, y, voronoi) * ScaleFactor;
}

float UProceduralTile::GetAmplitude(int x, int y, float ampl) {
	float G = 0.5;
	float f = 2.0;
	float a = 1.0;
	float t = 0.0;
	for (int i = 0; i < NumberOfOctaves; i++) {
		t += a * (FMath::PerlinNoise2D(*Seed + f * FVector2D{ x / static_cast<float>(TileSize) , y / static_cast<float>(TileSize) }) + 1) / 2;
		f *= 2.0;
		a *= G;
	}
	return t * ampl;
}