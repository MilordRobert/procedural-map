// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProceduralMeshComponent.h"
#include "GameFramework/Actor.h"
#include <ProceduralMap\Octave.h>
#include <ProceduralMap\Voronoi\VoronoiFeature.h>
#include <ProceduralMap\Voronoi\VoronoiFeatureInfo.h>
#include <ProceduralMap\Voronoi\VoronoiFeatureType.h>
#include <ProceduralMap\Voronoi\MinorVoronoiFeatureType.h>
#include <ProceduralMap\Voronoi\MinorVoronoiFeatureInfo.h>
#include <ProceduralMap\Voronoi\MinorVoronoiFeature.h>
#include <ProceduralMap\TileComponents.h>
#include <ProceduralMap\Voronoi\VoronoiLayer.h>
#include <ProceduralMap\ProceduralTile.h>
#include <RuntimeMeshActor.h>
#include <RuntimeMeshComponent/Public/Providers/RuntimeMeshProviderStatic.h>
#include <ProceduralMap\Biomes\BiomeLayer.h>
#include "ProceduralMapTile.generated.h"

UCLASS()
class PROCEDURALMAP_API AProceduralMapTile : public ARuntimeMeshActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actors properties
	AProceduralMapTile();

	UPROPERTY(EditAnywhere)
	int NumberOfOctaves{ 6 };

	UPROPERTY(EditAnywhere)
	int TileSize{ 64 };

	UPROPERTY(EditAnywhere)
	float FaceSize{ 200 };

	//UPROPERTY(VisibleAnywhere)
	//	//UProceduralMeshComponent* mesh;
	//URuntimeMeshComponentStatic* mesh;
	URuntimeMeshProviderStatic* StaticProvider;

	UPROPERTY(EditAnywhere)
	int VoronoiFeaturesPerTile{ 4 };
	
	UPROPERTY(EditAnywhere)
	int BiomesPerTile{ 4 };

	UPROPERTY(EditAnywhere)
	int MinorFeaturesPerVoronoiFeature{ 4 };

	UPROPERTY(EditAnywhere)
	int PerturbationFactor{ 20 };

	UPROPERTY(EditAnywhere)
	int PerturbationFrequency{ 8 };
	
	UPROPERTY(EditAnywhere)
	int BiomePerturbationFactor{ 15 };

	UPROPERTY(EditAnywhere)
	int BiomePerturbationFrequency{ 30 };

	UPROPERTY(EditAnywhere)
	float ScaleFactor{ 10000 };

	UPROPERTY(EditAnywhere)
	int NumberOfTiles{ 3 };

	UPROPERTY(EditAnywhere)
	UMaterialInterface* Material;

protected:
	
	UVoronoiLayer* voronoiLayer;
	UBiomeLayer* biomeLayer;

	FVector2D Seed;

	int NumberOfVertices;
	int NumberOfTilesSquared;
	int TotalNumberOfVoronoiFeatures;
	int TotalNumberOfBiomes;
	int TotalMapSize;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void UpdateDependentConstants();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void CreateMap();
};
