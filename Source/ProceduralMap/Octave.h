#pragma once
#include "Octave.generated.h"

USTRUCT()
struct FOctave {
	GENERATED_USTRUCT_BODY()

	int Frequency;

	float Amplitude;
};