// Fill out your copyright notice in the Description page of Project Settings.

#include "ProceduralMapTile.h"

// Sets default values
AProceduralMapTile::AProceduralMapTile() : Material(nullptr)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	StaticProvider = NewObject<URuntimeMeshProviderStatic>(this, TEXT("Provider"));
	//mesh = CreateDefaultSubobject<URuntimeMeshComponentStatic>(TEXT("Landscape"));
	//RootComponent = mesh;
	// New in UE 4.17, multi-threaded PhysX cooking.
	//mesh->bUseAsyncCooking = true;
	//mesh->bUseComplexAsSimpleCollision = true;
}

// Called when the game starts or when spawned
void AProceduralMapTile::BeginPlay()
{
	Super::BeginPlay();
	
}

void AProceduralMapTile::UpdateDependentConstants()
{
	NumberOfVertices = TileSize * TileSize;
	NumberOfTilesSquared = NumberOfTiles * NumberOfTiles;
	TotalNumberOfVoronoiFeatures = NumberOfTiles * VoronoiFeaturesPerTile;
	TotalNumberOfBiomes = NumberOfTiles * BiomesPerTile;
	TotalMapSize = NumberOfTiles * TileSize;
}

// Called every frame
void AProceduralMapTile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProceduralMapTile::CreateMap() {
	Seed = FVector2D{ FMath::FRand(), FMath::FRand() };
	FVector2D vSeed = FVector2D{ FMath::FRand(), FMath::FRand() };
	UpdateDependentConstants();
	biomeLayer = NewObject<UBiomeLayer>();
	voronoiLayer = NewObject<UVoronoiLayer>();
	biomeLayer->InitLayer(TotalNumberOfBiomes, FaceSize, TotalMapSize, BiomePerturbationFrequency, BiomePerturbationFactor, Seed);
	voronoiLayer->CreateLayer(TotalNumberOfVoronoiFeatures,
		MinorFeaturesPerVoronoiFeature,
		FaceSize,
		TotalMapSize,
		PerturbationFrequency,
		PerturbationFactor,
		vSeed,
		*biomeLayer);
	
	int totalTiles = NumberOfTiles * NumberOfTiles;
	TArray<UProceduralTile*> tiles;
	tiles.Reserve(totalTiles);
	GetRuntimeMeshComponent()->Initialize(StaticProvider);
	StaticProvider->SetupMaterialSlot(0, TEXT("TriMat"), Material);
	for (int i = 0; i < totalTiles; ++i) {
		tiles.Add(NewObject<UProceduralTile>());
	}
	ParallelFor(totalTiles, [&](int index) {
		int xOffset = (index / NumberOfTiles) * (TileSize - 1);
		int yOffset = (index % NumberOfTiles) * (TileSize - 1);

		UProceduralTile* tile = tiles[index];
		tile->CreateTile(xOffset, yOffset, TileSize, FaceSize, *voronoiLayer, *biomeLayer, Seed, ScaleFactor, NumberOfOctaves);
	}, false);
	for (int i = 0; i < totalTiles; ++i) {
		TArray<FRuntimeMeshTangent> tangents;
		UProceduralTile* tile = tiles[i];
		StaticProvider->CreateSectionFromComponents(0, i, 0, tile->Vertices, tile->Triangles, tile->Normals, tile->UVs, tile->Colors, tangents, ERuntimeMeshUpdateFrequency::Infrequent, true);
		//mesh->CreateSectionFromComponents(0, i, 0, tile->Vertices, tile->Triangles, tile->Normals, tile->UVs, colors, tangents, ERuntimeMeshUpdateFrequency::Infrequent, true);
	}
}